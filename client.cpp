//  cd "/home/anees/Documents/chatserverrepo/chatserverp/" && g++ client.cpp -o client -lboost_system && "/home/anees/Documents/chatserverrepo/chatserverp/"client
//uic -o client_ui.h client_ui.ui 

#include <iostream>
#include <boost/asio.hpp>
using namespace boost::asio;
using ip::tcp;
using std::string;
using namespace std;

#include "client_ui.h"

Ui::MainWindow ui;


int main(int argc,  char** argv)
{
    
    boost::asio::io_service io_service;
    //ui.chat_enter>text();

    //socket creation 
    tcp::socket socket(io_service);

    //connection
    socket.connect( tcp::endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 1234));

    //request/ send messages from client 
    string msg = "Hello from client!";
    
    string msgLength = to_string(msg.size());

    if(msg.size() < 0)
    {
        std::cout << "error" << std::endl;
        return(0);
    }
    boost::system::error_code error;
    
    socket.send(boost::asio::buffer(msgLength));
    //socket.send(boost::asio::buffer(msg));
    boost::asio::write( socket, boost::asio::buffer(msg), error );
    
    
    
    if( !error )
    {
        cout << "Client sent hello message!" << endl;
    }
    else
    {
        cout << "send failed: " << error.message() << endl;
    }

    return(0);
  
}





    







    
    /*
    #include <iostream>
    #include <string>
    #include <stdio.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <string.h>
    #include <netdb.h>
    #include <sys/uio.h>
    #include <sys/time.h>
    #include <sys/wait.h>
    #include <fcntl.h>
    #include <fstream>
    using namespace std;
    //Client side
    int main(int argc, char *argv[])
    {
        //we need 2 things: ip address and port number, in that order
        if(argc != 3)
        {
            cerr << "Usage: ip_address port" << endl; exit(0); 
        } //grab the IP address and port number 
        char *serverIp = argv[1]; int port = atoi(argv[2]); 
        //create a message buffer 
        char msg[1500]; 
        //setup a socket and connection tools 
        struct hostent* host = gethostbyname(serverIp); 
        sockaddr_in sendSockAddr;   
        bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
        sendSockAddr.sin_family = AF_INET; 
        sendSockAddr.sin_addr.s_addr = 
            inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
        sendSockAddr.sin_port = htons(port);
        int clientSd = socket(AF_INET, SOCK_STREAM, 0);
        //try to connect...
        int status = connect(clientSd,
                            (sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
        if(status < 0)
        {
            cout<<"Error connecting to socket!"<<endl; 
            return;
        }
        cout << "Connected to the server!" << endl;
        int bytesRead, bytesWritten = 0;
        struct timeval start1, end1;
        gettimeofday(&start1, NULL);
        while(1)
        {
            cout << ">";
            string data;
            getline(cin, data);
            memset(&msg, 0, sizeof(msg));//clear the buffer
            strcpy(msg, data.c_str());
            if(data == "exit")
            {
                send(clientSd, (char*)&msg, strlen(msg), 0);
                break;
            }
            bytesWritten += send(clientSd, (char*)&msg, strlen(msg), 0);
            cout << "Awaiting server response..." << endl;
            memset(&msg, 0, sizeof(msg));//clear the buffer
            bytesRead += recv(clientSd, (char*)&msg, sizeof(msg), 0);
            if(!strcmp(msg, "exit"))
            {
                cout << "Server has quit the session" << endl;
                break;
            }
            cout << "Server: " << msg << endl;
        }
        gettimeofday(&end1, NULL);
        close(clientSd);
        cout << "********Session********" << endl;
        cout << "Bytes written: " << bytesWritten << 
        " Bytes read: " << bytesRead << endl;
        cout << "Elapsed time: " << (end1.tv_sec- start1.tv_sec) 
        << " secs" << endl;
        cout << "Connection closed" << endl;
        return 0;    
    }

    */