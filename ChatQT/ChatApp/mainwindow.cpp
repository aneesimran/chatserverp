#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "loginwindow.h"

#include <iostream>
#include <boost/asio.hpp>
#include <sstream>
using namespace boost::asio;
using ip::tcp;
using std::string;
using namespace std;
string username;
string checkName = "1b";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

     ui->msgBox->setEnabled(false);
     ui->sendButton->setEnabled(false);
     ui->chatList->setEnabled(false);
     ui->friendsList->setEnabled(false);
     ui->findBtn->setEnabled(false);
     ui->friendSearch->setEnabled(false);
     ui->startChatBtn->setEnabled(false);
     ui->friendresultBox->setEnabled(false);
     ui->addFriendBtn->setEnabled(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_loginBtn_clicked()
{
    QString username = ui->usernameBox->text();

    QString password = ui ->passwordBox->text();

    std::string usernameAns = username.toUtf8().constData();
    std::string passwordAns = password.toUtf8().constData();

    string type = "login";
    string string1 = usernameAns;
    string string2 = passwordAns;
    mThread = new MyThread(this,  type, string1, string2);
    connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

    mThread->start();


    UserAccountCheck(usernameAns, passwordAns);
}

void MainWindow::UserAccountCheck(std::string username, std::string password)
{
    cout<<"this is the username: "<<username<<endl;
    cout<<"this is the password; "<<password<<endl;
}


void MainWindow::on_registerBtn_clicked()
{
    QString username = ui->usernameBox->text();

    QString password = ui ->passwordBox->text();

    std::string usernameAns = username.toUtf8().constData();
    std::string passwordAns = password.toUtf8().constData();

    string type = "register";
    string string1 = usernameAns;
    string string2 = passwordAns;
    mThread = new MyThread(this,  type, string1, string2);
    connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

    mThread->start();
    //mThread->Stop = true;

    ServerConnection();
    UserAccountCheck(usernameAns, passwordAns);
}

void MainWindow::ServerConnection()
{
    string nothing = "";
}

void MainWindow::onChatServer(QString qType, QString ans, QString str1, QString str2)
{
    string strAns = ans.toUtf8().constData();
    string strStr1 = str1.toUtf8().constData();
    string strStr2 = str2.toUtf8().constData();
    string type = qType.toUtf8().constData();

    if(type == "login")
    {
        if(strAns == "loginpass")
        {
            cout<<"success"<<endl;
            string msgboxStr = strStr1 + " has logged in";
            username = strStr1;
            QString qMsg = QString::fromStdString(msgboxStr);
            ui ->userStatusbox->setText(qMsg);
            ui->loginBtn->setEnabled(false);
            ui->registerBtn->setEnabled(false);
            ui->usernameBox->setEnabled(false);
            ui->passwordBox->setEnabled(false);
            ui->friendSearch->setEnabled(true);
            ui->findBtn->setEnabled(true);
            ui->friendsList->setEnabled(true);
            ui->startChatBtn->setEnabled(true);
            ui->friendresultBox->setEnabled(true);

            string type = "friendshow";
            string string1 = username;
            string string2 = "e";
            mThread = new MyThread(this,  type, string1, string2);
            connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

            mThread->start();


        }
        else
        {
            cout<<"fail"<<endl;
            string msgboxStr = "Login failed... check login details";
            QString qMsg = QString::fromStdString(msgboxStr);
            ui ->userStatusbox->setText(qMsg);
        }
    }

    else if(type == "register")
    {
        if(strAns == "registerpass")
        {
            string msgboxStr = strStr1 + " has registered";
            QString qMsg = QString::fromStdString(msgboxStr);
            ui ->userStatusbox->setText(qMsg);
            ui->loginBtn->setEnabled(false);
            ui->registerBtn->setEnabled(false);
            ui->usernameBox->setEnabled(false);
            ui->passwordBox->setEnabled(false);
            ui->friendsList->setEnabled(true);
            ui->friendSearch->setEnabled(true);
            ui->findBtn->setEnabled(true);
            ui->startChatBtn->setEnabled(true);
            ui->friendresultBox->setEnabled(true);

            string type = "friendshow";
            string string1 = username;
            string string2 = "e";
            mThread = new MyThread(this,  type, string1, string2);
            connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

            mThread->start();
        }
        else
        {
            string msgboxStr = "Username exists";
            QString qMsg = QString::fromStdString(msgboxStr);
            ui ->userStatusbox->setText(qMsg);
        }
    }

    else if(type == "friendsearch")
    {
        if(strAns == "friendpass")
        {
            string friendmsgbox = "user " + strStr1 + " is found";
            QString friendmsgAns = QString::fromStdString(friendmsgbox);
            ui->friendresultBox->setText(friendmsgAns);
            ui->addFriendBtn->setEnabled(true);
        }
        else
        {
            string friendmsgbox = "user is not found";
            QString friendmsgAns = QString::fromStdString(friendmsgbox);
            ui->friendresultBox->setText(friendmsgAns);
        }
    }

    else if(type == "addfriend")
    {
        if(strAns == "friendaddpass")
        {
            string friendAns = strStr2 + " is now friends with " + strStr1;
            QString friendsStatus = QString::fromStdString(friendAns);
            ui->userStatusbox->setText(friendsStatus);

            while(ui->friendsList->count() > 0 )
            {
                ui->friendsList->takeItem(0);
            }

            string type = "friendshow";
            string string1 = username;
            string string2 = "e";
            mThread = new MyThread(this,  type, string1, string2);
            connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

            mThread->start();
        }
        else
        {
            string friendAns = "you are already friends with this person";
            QString friendsStatus = QString::fromStdString(friendAns);
            ui->userStatusbox->setText(friendsStatus);
        }
    }

    else if(type == "friendshow")
    {
        stringstream toInt(strStr1);
        int friendCount = 0;
        toInt >> friendCount;
        if(friendCount > 0)
        {
            friendCount = friendCount - 1;
        }

        cout <<"friend count: "<< friendCount << endl;

        string type = "friendgetName";
        string string1 = username;
        string string2 = std::to_string(friendCount);
        mThread = new MyThread(this,  type, string1, string2);
        connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

        mThread->start();


    }

    else if(type == "friendgetName")
    {
        cout <<"get user :"<< strStr2 << endl;
        QString friendNameQ = QString::fromStdString(strStr2);
        ui ->friendsList->addItem(friendNameQ);

        stringstream toInt(strStr1);
        int friendCount = 0;
        toInt >> friendCount;
        friendCount = friendCount - 1;
        cout <<"friend count: "<< friendCount << endl;



        if(friendCount < 0)
        {
            cout << "all friends done" << endl;
        }

       else
        {
            string type = "friendgetName";
            string string1 = username;
            string string2 = std::to_string(friendCount);
            mThread = new MyThread(this,  type, string1, string2);
            connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

            mThread->start();
        }
    }

    else if(type == "startChat")
    {
        cout << "this is a test for the if statement" << endl;
    }

}

void MainWindow::on_findBtn_clicked()
{

    QString friendQ = ui->friendSearch->text();

    string string1 = friendQ.toUtf8().constData();
    string string2 = "empty";
    string type = "friendsearch";

    mThread = new MyThread(this,  type, string1, string2);
    connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

    mThread->start();

}

void MainWindow::on_addFriendBtn_clicked()
{
    string string1 = username;
    string string2 = (ui->friendSearch->text()).toUtf8().constData();
    string type = "addfriend";

    mThread = new MyThread(this,  type, string1, string2);
    connect(mThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

    mThread->start();
}

void MainWindow::on_startChatBtn_clicked()
{

    QString fNameQ = ui->friendsList->currentItem()->text();
    string fNameStr = fNameQ.toUtf8().constData();
    cout << fNameStr << endl;

    string friendName = fNameStr;
    string message = "blank";


    msgThread = new messageThread(this, "startChat", friendName, message, username);
    connect(msgThread, SIGNAL(ChatServer(QString, QString, QString, QString)), this, SLOT(onChatServer(QString, QString, QString, QString)));

    msgThread->start();
}
