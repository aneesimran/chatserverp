#ifndef MESSAGETHREAD_H
#define MESSAGETHREAD_H

#include <QThread>
using std::string;
using namespace std;

class messageThread : public QThread
{
    Q_OBJECT
public:
    explicit messageThread(QObject *parent = 0, string type = "", string string1 = "", string string2 = "", string string3 = "");
    void run();
    bool Stop;
signals:
    void ChatServer(QString, QString, QString, QString);
public slots:

};


#endif // MESSAGETHREAD_H
