#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
using std::string;
using namespace std;

class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit MyThread(QObject *parent = 0, string type = "", string string1 = "", string string2 = "");
    void run();
    bool Stop;
signals:
    void ChatServer(QString, QString, QString, QString);
public slots:

};

#endif // MYTHREAD_H
