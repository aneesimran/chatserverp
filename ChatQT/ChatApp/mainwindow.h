#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include "mythread.h"
#include "messagethread.h"


#include <iostream>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    MyThread *mThread;
    messageThread *msgThread;

private slots:


    void on_loginBtn_clicked();

    void on_registerBtn_clicked();


    void on_findBtn_clicked();

    void on_addFriendBtn_clicked();

    void on_startChatBtn_clicked();

public slots:
    void onChatServer(QString, QString, QString, QString);


private:
    Ui::MainWindow *ui;
    void UserAccountCheck(std::string, std::string);
    void ServerConnection();
};

#endif // MAINWINDOW_H
