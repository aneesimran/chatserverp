#include "messagethread.h"

#include <QtCore>

#include <iostream>
#include <boost/asio.hpp>
using namespace boost::asio;
using ip::tcp;
using std::string;
using namespace std;

 string typeMsg;
 string msgTo;
 string message;
 string msgFrom;

messageThread::messageThread(QObject *parent, string typeMSGAns, string msgToAns, string messageAns, string msgFromAns)
{
    typeMsg = typeMSGAns;
    msgTo = msgToAns;
    message = messageAns;
    msgFrom = msgFromAns;
}

void messageThread::run()
{

    string runn = "run";

    boost::asio::io_service io_service;
    //ui.chat_enter>text();

    //socket creation
    tcp::socket socket(io_service);

    //connection
    socket.connect( tcp::endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 1234));

    //request/ send messages from client

    boost::system::error_code error;

    int msgCount = 0;
    if(typeMsg == "startChat")
    {
        cout << "enter here" <<endl;
        const int size = 1024;
        char dta1[size];
        string typeMsg1 = typeMsg;
        string msgTo1 = msgTo;
        string message1 = message;
        string msgFrom1 = msgFrom;

        socket.send(boost::asio::buffer(typeMsg1));
        socket.send(boost::asio::buffer(msgTo1));
        socket.send(boost::asio::buffer(message1));
        socket.send(boost::asio::buffer(msgFrom1));

        socket.read_some(boost::asio::buffer(dta1, size));

        stringstream toInt(dta1);

        toInt >> msgCount;

        cout << msgCount << endl;

        std::fill(std::begin(dta1), std::end(dta1), '\0');
        typeMsg = "showMsgHist";
    }

    while(true)
    {

        const int size = 1024;
        char data1[size];
        char data2[size];
        char data3[size];

        cout << "the type message is " << typeMsg << endl;
        socket.send(boost::asio::buffer(typeMsg));
        cout << "pt 2"<< endl;
        cout << msgTo<< endl;
        socket.send(boost::asio::buffer(msgTo));
        socket.read_some(boost::asio::buffer(data1, size));
        std::fill(std::begin(data1), std::end(data1), '\0');
        cout << "pt 3"<< endl;
        socket.send(boost::asio::buffer(message));
        socket.read_some(boost::asio::buffer(data1, size));
        std::fill(std::begin(data1), std::end(data1), '\0');
        cout << "pt 4"<< endl;
        socket.send(boost::asio::buffer(msgFrom));
        cout << "pt 5"<< endl;

        if(typeMsg == "showMsgHist")
        {
            msgCount = msgCount - 1;
            socket.send(boost::asio::buffer(std::to_string(msgCount)));
        }

        socket.read_some(boost::asio::buffer(data1, size));
        cout<<"data 1: "<<data1<<endl;

        socket.send(boost::asio::buffer("blocker"));

        socket.read_some(boost::asio::buffer(data2, size));
        cout<<"---------------------"<<endl;
        cout<<"data 2: "<<data2<<endl;
        socket.send(boost::asio::buffer("blocker"));

        socket.read_some(boost::asio::buffer(data3, size));
        cout<<"---------------------"<<endl;
        cout<<"data 3: "<<data3<<endl;

        QString ans = data1;
        QString str1 = data2;
        QString str2 = data3;
        QString qType= QString::fromStdString(typeMsg);

        std::fill(std::begin(data1), std::end(data1), '\0');
        std::fill(std::begin(data2), std::end(data2), '\0');
        std::fill(std::begin(data3), std::end(data3), '\0');

        if(data1 == "endMessage1")
        {
            cout << "breaking the loop" << endl;
            break;
        }

        if(this->Stop) break;


        emit ChatServer(qType, ans, str1, str2);
    }
}
