#-------------------------------------------------
#
# Project created by QtCreator 2019-10-30T14:15:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ChatApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    loginwindow.cpp \
    mythread.cpp \
    messagethread.cpp

HEADERS  += mainwindow.h \
    loginwindow.h \
    mythread.h \
    messagethread.h

FORMS    += mainwindow.ui \
    loginwindow.ui

LIBS += -L/usr/include/boost -lboost_system
LIBS += -L/usr/include/boost -lboost_chrono
LIBS += -L/usr/include/boost -lboost_thread
LIBS += -L/usr/include/boost -lboost_timer
