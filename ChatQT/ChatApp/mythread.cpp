#include "mythread.h"
#include <QtCore>

#include <iostream>
#include <boost/asio.hpp>
using namespace boost::asio;
using ip::tcp;
using std::string;
using namespace std;

 string type;
 string string1;
 string string2;

MyThread::MyThread(QObject *parent, string typeAns, string string1Ans, string string2Ans):
        QThread(parent)
{
    type = typeAns;
    string1 = string1Ans;
    string2 = string2Ans;
}

void MyThread::run()
{
    QMutex mutex;
    mutex.lock();
    boost::asio::io_service io_service;
    //ui.chat_enter>text();

     //socket creation
     tcp::socket socket(io_service);

     //connection
     socket.connect( tcp::endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 1234));

     //request/ send messages from client

     boost::system::error_code error;

     const int size = 1024;
     char data1[size];
     char data2[size];
     char data3[size];



        //socket.send(boost::asio::buffer(msgLength));

     socket.send(boost::asio::buffer(type));
     socket.send(boost::asio::buffer(string1));
     socket.send(boost::asio::buffer(string2));

        //boost::asio::write( socket, boost::asio::buffer(type), error );
        //boost::asio::write( socket, boost::asio::buffer(string1), error );
        //boost::asio::write( socket, boost::asio::buffer(string2), error );

        if( !error )
        {
            cout << "Client sent hello message!" << endl;
        }
        else
        {
            cout << "send failed: " << error.message() << endl;
        }

        //boost::asio::streambuf receive_buffer;
        //boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);

        socket.read_some(boost::asio::buffer(data1, size));
        cout<<"data 1: "<<data1<<endl;

        socket.send(boost::asio::buffer("blocker"));

        socket.read_some(boost::asio::buffer(data2, size));
        cout<<"---------------------"<<endl;
        cout<<"data 2: "<<data2<<endl;

        socket.send(boost::asio::buffer("blocker"));

        socket.read_some(boost::asio::buffer(data3, size));
        cout<<"---------------------"<<endl;
        cout<<"data 3: "<<data3<<endl;

        QString ans = data1;
        QString str1 = data2;
        QString str2 = data3;
        QString qType= QString::fromStdString(type);

        std::fill(std::begin(data1), std::end(data1), '\0');
        std::fill(std::begin(data2), std::end(data2), '\0');
        std::fill(std::begin(data3), std::end(data3), '\0');

        mutex.unlock();

    emit ChatServer(qType, ans, str1, str2);
}
