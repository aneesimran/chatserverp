/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionRegister;
    QAction *actionLogin;
    QAction *actionQuit;
    QWidget *centralWidget;
    QListWidget *chatList;
    QLineEdit *msgBox;
    QPushButton *sendButton;
    QLineEdit *usernameBox;
    QLineEdit *passwordBox;
    QPushButton *registerBtn;
    QPushButton *loginBtn;
    QLineEdit *friendSearch;
    QPushButton *findBtn;
    QLineEdit *userStatusbox;
    QPushButton *addFriendBtn;
    QLineEdit *friendresultBox;
    QListWidget *friendsList;
    QPushButton *startChatBtn;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(806, 640);
        actionRegister = new QAction(MainWindow);
        actionRegister->setObjectName(QStringLiteral("actionRegister"));
        actionLogin = new QAction(MainWindow);
        actionLogin->setObjectName(QStringLiteral("actionLogin"));
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        chatList = new QListWidget(centralWidget);
        chatList->setObjectName(QStringLiteral("chatList"));
        chatList->setGeometry(QRect(270, 40, 521, 511));
        QFont font;
        font.setPointSize(8);
        chatList->setFont(font);
        msgBox = new QLineEdit(centralWidget);
        msgBox->setObjectName(QStringLiteral("msgBox"));
        msgBox->setGeometry(QRect(270, 560, 461, 22));
        sendButton = new QPushButton(centralWidget);
        sendButton->setObjectName(QStringLiteral("sendButton"));
        sendButton->setGeometry(QRect(739, 560, 51, 22));
        usernameBox = new QLineEdit(centralWidget);
        usernameBox->setObjectName(QStringLiteral("usernameBox"));
        usernameBox->setGeometry(QRect(10, 10, 121, 22));
        passwordBox = new QLineEdit(centralWidget);
        passwordBox->setObjectName(QStringLiteral("passwordBox"));
        passwordBox->setGeometry(QRect(140, 10, 121, 22));
        passwordBox->setEchoMode(QLineEdit::Password);
        registerBtn = new QPushButton(centralWidget);
        registerBtn->setObjectName(QStringLiteral("registerBtn"));
        registerBtn->setGeometry(QRect(270, 10, 61, 22));
        loginBtn = new QPushButton(centralWidget);
        loginBtn->setObjectName(QStringLiteral("loginBtn"));
        loginBtn->setGeometry(QRect(340, 10, 61, 22));
        friendSearch = new QLineEdit(centralWidget);
        friendSearch->setObjectName(QStringLiteral("friendSearch"));
        friendSearch->setGeometry(QRect(10, 500, 201, 22));
        findBtn = new QPushButton(centralWidget);
        findBtn->setObjectName(QStringLiteral("findBtn"));
        findBtn->setGeometry(QRect(220, 500, 41, 22));
        userStatusbox = new QLineEdit(centralWidget);
        userStatusbox->setObjectName(QStringLiteral("userStatusbox"));
        userStatusbox->setGeometry(QRect(592, 10, 201, 22));
        userStatusbox->setFont(font);
        userStatusbox->setReadOnly(true);
        addFriendBtn = new QPushButton(centralWidget);
        addFriendBtn->setObjectName(QStringLiteral("addFriendBtn"));
        addFriendBtn->setGeometry(QRect(10, 560, 251, 22));
        friendresultBox = new QLineEdit(centralWidget);
        friendresultBox->setObjectName(QStringLiteral("friendresultBox"));
        friendresultBox->setGeometry(QRect(10, 530, 251, 22));
        friendresultBox->setReadOnly(true);
        friendsList = new QListWidget(centralWidget);
        friendsList->setObjectName(QStringLiteral("friendsList"));
        friendsList->setGeometry(QRect(10, 40, 251, 421));
        startChatBtn = new QPushButton(centralWidget);
        startChatBtn->setObjectName(QStringLiteral("startChatBtn"));
        startChatBtn->setGeometry(QRect(10, 470, 251, 22));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 806, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionRegister->setText(QApplication::translate("MainWindow", "Register", 0));
        actionLogin->setText(QApplication::translate("MainWindow", "Login", 0));
        actionQuit->setText(QApplication::translate("MainWindow", "Quit", 0));
        msgBox->setPlaceholderText(QApplication::translate("MainWindow", "Type your message here...", 0));
        sendButton->setText(QApplication::translate("MainWindow", "Send", 0));
        usernameBox->setPlaceholderText(QApplication::translate("MainWindow", "username", 0));
        passwordBox->setPlaceholderText(QApplication::translate("MainWindow", "password", 0));
        registerBtn->setText(QApplication::translate("MainWindow", "Register", 0));
        loginBtn->setText(QApplication::translate("MainWindow", "Login", 0));
        friendSearch->setPlaceholderText(QApplication::translate("MainWindow", "Find Friend", 0));
        findBtn->setText(QApplication::translate("MainWindow", "Find", 0));
        addFriendBtn->setText(QApplication::translate("MainWindow", "Add Friend", 0));
        startChatBtn->setText(QApplication::translate("MainWindow", "Chat", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
