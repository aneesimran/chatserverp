#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <boost/asio.hpp>
using namespace boost::asio;
using ip::tcp;
using std::string;
using namespace std;


//  cd "/home/anees/Documents/chatserverrepo/chatserverp/" && g++ client.cpp -o client -lboost_system && "/home/anees/Documents/chatserverrepo/chatserverp/"client
//  uic -o client_ui.h client_ui.ui

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    boost::asio::io_service io_service;


    //socket creation
    tcp::socket socket(io_service);

    //connection
    socket.connect( tcp::endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 1234));

    //request/ send messages from client
    string msg = "Hello from client!";

    string msgLength = to_string(msg.size());

    if(msg.size() < 0)
    {
        std::cout << "error" << std::endl;
        //return(0);
    }
    boost::system::error_code error;

    socket.send(boost::asio::buffer(msgLength));
    //socket.send(boost::asio::buffer(msg));
    boost::asio::write( socket, boost::asio::buffer(msg), error );

    if( !error )
    {
        cout << "Client sent hello message!" << endl;

        //ui->msgBox->setText("Client sent hello message!");
    }
    else
    {
        cout << "send failed: " << error.message() << endl;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


