#ifndef USERAUTHENTICATIONSCREEN_H
#define USERAUTHENTICATIONSCREEN_H

#include <QDialog>

namespace Ui {
class UserAuthenticationScreen;
}

class UserAuthenticationScreen : public QDialog
{
    Q_OBJECT

public:
    explicit UserAuthenticationScreen(QWidget *parent = 0);
    ~UserAuthenticationScreen();

private slots:
    void on_pushButton_clicked();

private:
    Ui::UserAuthenticationScreen *ui;
};

#endif // USERAUTHENTICATIONSCREEN_H
