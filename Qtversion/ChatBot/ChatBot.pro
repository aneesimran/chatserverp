#-------------------------------------------------
#
# Project created by QtCreator 2019-10-28T10:23:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ChatBot
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

LIBS += -L/user/include/boost -lboost_system
LIBS += -L/user/include/boost -lboost_chrono
LIBS += -L/user/include/boost -lboost_thread
LIBS += -L/user/include/boost -lboost_timer
