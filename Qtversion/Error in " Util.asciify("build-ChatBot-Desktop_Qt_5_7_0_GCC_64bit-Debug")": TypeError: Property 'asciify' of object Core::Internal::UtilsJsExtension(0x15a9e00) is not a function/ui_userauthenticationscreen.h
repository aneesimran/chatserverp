/********************************************************************************
** Form generated from reading UI file 'userauthenticationscreen.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERAUTHENTICATIONSCREEN_H
#define UI_USERAUTHENTICATIONSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_UserAuthenticationScreen
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;

    void setupUi(QDialog *UserAuthenticationScreen)
    {
        if (UserAuthenticationScreen->objectName().isEmpty())
            UserAuthenticationScreen->setObjectName(QStringLiteral("UserAuthenticationScreen"));
        UserAuthenticationScreen->resize(400, 300);
        pushButton = new QPushButton(UserAuthenticationScreen);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(160, 230, 80, 22));
        pushButton_2 = new QPushButton(UserAuthenticationScreen);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(160, 190, 80, 22));
        lineEdit = new QLineEdit(UserAuthenticationScreen);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(80, 150, 241, 22));
        lineEdit_2 = new QLineEdit(UserAuthenticationScreen);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(80, 100, 241, 22));

        retranslateUi(UserAuthenticationScreen);

        QMetaObject::connectSlotsByName(UserAuthenticationScreen);
    } // setupUi

    void retranslateUi(QDialog *UserAuthenticationScreen)
    {
        UserAuthenticationScreen->setWindowTitle(QApplication::translate("UserAuthenticationScreen", "Dialog", 0));
        pushButton->setText(QApplication::translate("UserAuthenticationScreen", "Login", 0));
        pushButton_2->setText(QApplication::translate("UserAuthenticationScreen", "Register", 0));
    } // retranslateUi

};

namespace Ui {
    class UserAuthenticationScreen: public Ui_UserAuthenticationScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERAUTHENTICATIONSCREEN_H
