/********************************************************************************
** Form generated from reading UI file 'chatscreen.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHATSCREEN_H
#define UI_CHATSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_ChatScreen
{
public:
    QListView *friendList;
    QListView *messageBox;
    QLineEdit *msgBox;
    QPushButton *sendButton;

    void setupUi(QDialog *ChatScreen)
    {
        if (ChatScreen->objectName().isEmpty())
            ChatScreen->setObjectName(QStringLiteral("ChatScreen"));
        ChatScreen->resize(688, 611);
        friendList = new QListView(ChatScreen);
        friendList->setObjectName(QStringLiteral("friendList"));
        friendList->setGeometry(QRect(20, 20, 171, 571));
        messageBox = new QListView(ChatScreen);
        messageBox->setObjectName(QStringLiteral("messageBox"));
        messageBox->setGeometry(QRect(200, 20, 471, 541));
        msgBox = new QLineEdit(ChatScreen);
        msgBox->setObjectName(QStringLiteral("msgBox"));
        msgBox->setGeometry(QRect(200, 570, 401, 21));
        sendButton = new QPushButton(ChatScreen);
        sendButton->setObjectName(QStringLiteral("sendButton"));
        sendButton->setGeometry(QRect(610, 570, 61, 22));

        retranslateUi(ChatScreen);

        QMetaObject::connectSlotsByName(ChatScreen);
    } // setupUi

    void retranslateUi(QDialog *ChatScreen)
    {
        ChatScreen->setWindowTitle(QApplication::translate("ChatScreen", "Dialog", 0));
        sendButton->setText(QApplication::translate("ChatScreen", "Send", 0));
    } // retranslateUi

};

namespace Ui {
    class ChatScreen: public Ui_ChatScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHATSCREEN_H
