import socket
import select
import errno
import sys
import pymongo
import getpass

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb1 = myclient["mydatabase1"]
mycolcurrentUser = mydb1["UserAccounts1"]

HEADER_LENGTH = 10

IP = "127.0.0.1"
PORT = 1234
loginPrompt = input("Would you like to login? (y/n): ")

# Create a socket
# socket.AF_INET - address family, IPv4, some otehr possible are AF_INET6, AF_BLUETOOTH, AF_UNIX
# socket.SOCK_STREAM - TCP, conection-based, socket.SOCK_DGRAM - UDP, connectionless, datagrams, socket.SOCK_RAW - raw IP packets
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to a given ip and port
client_socket.connect((IP, PORT))

# Set connection to non-blocking state, so .recv() call won;t block, just return some exception we'll handle
client_socket.setblocking(False)

# Prepare username and header and send them
# We need to encode username to bytes, then count number of bytes and prepare header of fixed size, that we encode to byte;s as well

loginAns = loginPrompt.encode('utf-8')
print("this is the login ans encoded: ", loginAns )
loginAns_header = f"{len(loginAns):<{HEADER_LENGTH}}".encode('utf-8')
print("this is the loginans len: ", len(loginAns))
print("this is the login header: ", loginAns_header)
client_socket.send(loginAns_header + loginAns)

checkUser = False

if loginPrompt == "y":
    my_username = input("Enter Username: ")
    username = my_username.encode('utf-8')
    username_header = f"{len(username):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(username_header + username)
    mycolcurrentUser = mydb1["UserAccounts1"]
    my_password = getpass.getpass("Enter your password: ")
    password = my_password.encode('utf-8')
    password_header = f"{len(password):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(password_header + password)
    
    usernameQuery = {"username": my_username, "password": my_password}
    mydoc = mycolcurrentUser.find(usernameQuery)
    checkUser = False
    for x in mydoc:
        print("Successs")
        print(x)
        checkUser = True
    if checkUser == False:
        print("user not found")
        sys.exit(0)


elif loginPrompt == "n":
    my_username = input("Enter Username: ")
    username = my_username.encode('utf-8')
    username_header = f"{len(username):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(username_header + username)
    mycolcurrentUser = mydb1["UserAccounts1"]
    my_password = getpass.getpass("Enter your password: ")
    password = my_password.encode('utf-8')
    password_header = f"{len(password):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(password_header + password)

    userExistCheck = False
    usernameQuery = {"username": username}
    mydoc = mycolcurrentUser.find(usernameQuery)
    for x in mydoc:
        userExistCheck = True
        print("user found in the for loop")
    if userExistCheck == False:
        print("User added")
    elif userExistCheck == True:
        print("User exists")
        sys.exit(0)






checkAns = False
nameCol = mydb1["UserAccounts1"]
myquery = {"username": my_username}
mydoc = nameCol.find(myquery)
if x in mydoc:
    checkAns = True

if checkAns == True:
    while True:
        print(" ")
        print("Welcome back " + my_username)
        print("Choose the following options: ")
        print("0. Start messaging")
        print("1. View unread chat")
        print("2. Delete user")
        print("3. View Chat History")
        print("At any time type type 'status1' for options" )
        optionAns = input(": ")

        if optionAns == "0":
            break
        if optionAns == "1":
            mycolcurrentChatUser = mydb1["UserChats" + my_username]
            myquery = {"Message Status": 0}
            mydoc = mycolcurrentChatUser.find(myquery, {'_id': False})
            for x in mydoc:
                print(x)
                messageQuery = {"Name": x["Name"], "Message": x["Message"], "Date and time": x["Date and time"], "Message Status": x["Message Status"]}
                newValues = {"$set": {"Message Status": 1}}
                mycolcurrentChatUser.update_one(messageQuery, newValues)

        if optionAns == "2":
            
            mycolcurrentUser = mydb1["UserAccounts1"]
            for a in mycolcurrentUser.find():
                print(a)
            deleteQuery = {"username": my_username}
            x = mycolcurrentUser.delete_one(deleteQuery)
            print(" ...deleting")
            print("User deleted.")
            print("Quitting...")
        
        
            

while True:

    # Wait for user to input a message
    message = input(f'{my_username} > ')
    #message = ""

    # If message is not empty - send it
    if message:

        if message == "status1":
            print("Choose the following options: ")
            print("1. Appear Online")
            print("2. Appear Offline")
            print("3. Quit chat and go offline")
            print("4. Go back to chat")
            statusAns = input(": ")
            if statusAns == "1":
                status = statusAns.encode('utf-8')
                status_header = f"{len(status):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(status_header + status)
            if statusAns == "2":
                status = statusAns.encode('utf-8')
                status_header = f"{len(status):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(status_header + status)
            if statusAns == "3":
                status = statusAns.encode('utf-8')
                status_header = f"{len(status):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(status_header + status)
                print("Quitting chat")
                sys.exit(0)
            if status == "4":
                status = statusAns.encode('utf-8')
                status_header = f"{len(status):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(status_header + status)
                print("")
    
        statusAns = "0"
        status = statusAns.encode('utf-8')
        status_header = f"{len(status):<{HEADER_LENGTH}}".encode('utf-8')
        client_socket.send(status_header + status)

        # Encode message to bytes, prepare header and convert to bytes, like for username above, then send
        message = message.encode('utf-8')
        message_header = f"{len(message):<{HEADER_LENGTH}}".encode('utf-8')
        client_socket.send(message_header + message)

    try:
        # Now we want to loop over received messages (there might be more than one) and print them
        while True:

            # Receive our "header" containing username length, it's size is defined and constant
            username_header = client_socket.recv(HEADER_LENGTH)

            # If we received no data, server gracefully closed a connection, for example using socket.close() or socket.shutdown(socket.SHUT_RDWR)
            if not len(username_header):
                print('Connection closed by the server')
                sys.exit()

            # Convert header to int value
            username_length = int(username_header.decode('utf-8').strip())

            # Receive and decode username
            username = client_socket.recv(username_length).decode('utf-8')

            # Now do the same for message (as we received username, we received whole message, there's no need to check if it has any length)
            message_header = client_socket.recv(HEADER_LENGTH)
            message_length = int(message_header.decode('utf-8').strip())
            message = client_socket.recv(message_length).decode('utf-8')

            # Print message
            print(f'{username} > {message}')

    except IOError as e:
        # This is normal on non blocking connections - when there are no incoming data error is going to be raised
        # Some operating systems will indicate that using AGAIN, and some using WOULDBLOCK error code
        # We are going to check for both - if one of them - that's expected, means no incoming data, continue as normal
        # If we got different error code - something happened
        if e.errno != errno.EAGAIN and e.errno != errno.EWOULDBLOCK:
            print('Reading error: {}'.format(str(e)))
            sys.exit()

        # We just did not receive anything
        continue

    except Exception as e:
        # Any other exception - something happened, exit
        print('Reading error: '.format(str(e)))
        sys.exit()



# # Pyhton program to implement client side of the chat room 
# import socket
# import select
# import sys

# server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# if len(sys.argv) != 3:
#     print("Correct usage: script, IP address, port number")
#     exit()
# IP_address = str(sys.argv[1])
# Port = int(sys.argv[2])
# server.connect((IP_address, Port))

# while True:

#     # maintains a list of possible input streams

#     sockets_list = [sys.stdin, server]

#     """ There are two possible input situations. Either the user wants to give manual input to send to
#     other people or the server is sending a message to be printed on the screen. Select returns from socket_list,
#     the stream that is reader for input. So for example, if the server wants to send a message, then the if
#     condition will hold the true below. If the user wnats to send a message, the else condition will evaluate as true"""

#     read_sockets, write_socket, error_socket = select.select(sockets_list,[],[])

#     for socks in read_sockets:
#         if socks == server:
#             message = socks.recv(2048)
#             print(message)
#         else:
#             message = sys.stdin.readline()
#             server.send(message)
#             sys.stdout.write("<You>")
#             sys.stdout.write(message)
#             sys.stdout.flush()

# server.close()



