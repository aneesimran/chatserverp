import socket
import sys
import select
import pymongo
from datetime import datetime


myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb1 = myclient["mydatabase1"]

mycol = mydb1["UserChats"]



dblist = myclient.list_database_names()
if "mydatabase" in dblist:
    print("The database exists")
else:
    print("error not found")


HEADER_LENGTH = 10

IP = "127.0.0.1"
PORT = 1234

# Create a socket
# socket.AF_INET - address family, IPv4, some otehr possible are AF_INET6, AF_BLUETOOTH, AF_UNIX
# socket.SOCK_STREAM - TCP, conection-based, socket.SOCK_DGRAM - UDP, connectionless, datagrams, socket.SOCK_RAW - raw IP packets
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# SO_ - socket option
# SOL_ - socket option level
# Sets REUSEADDR (as a socket option) to 1 on socket
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind, so server informs operating system that it's going to use given IP and port
# For a server using 0.0.0.0 means to listen on all available interfaces, useful to connect locally to 127.0.0.1 and remotely to LAN interface IP
server_socket.bind((IP, PORT))

# This makes server listen to new connections
server_socket.listen(6)

# List of sockets for select.select()
sockets_list = [server_socket]

# List of connected clients - socket as a key, user header and name as data
clients = {}

print(f'Listening for connections on {IP}:{PORT}...')

# Handles message receiving

def receive_message(client_socket):
    
    try:
        print("\n -------STARTED-------\n")
        # Receive our "header" containing message length, it's size is defined and constant
        message_header = client_socket.recv(HEADER_LENGTH)
        print("message header: ", message_header)
        # If we received no data, client gracefully closed a connection, for example using socket.close() or socket.shutdown(socket.SHUT_RDWR)
        if not len(message_header):
            print("it is in this if statement")
            message_header = "1"
            message_header = message_header.encode('utf-8')
            #return False
            

        # Convert header to int value
        message_length = int(message_header.decode('utf-8'))
        print("message length: ", message_length)

        #messageHeaderans = message_header
        messageDataans = client_socket.recv(message_length)

        # Return an object of message header and message data
        return {'header': message_length, 'data': messageDataans}
        

        

    except Exception as e:

        print("the exception is: ", e)
        # If we are here, client closed connection violently, for example by pressing ctrl+c on his script
        # or just lost his connection
        # socket.close() also invokes socket.shutdown(socket.SHUT_RDWR) what sends information about closing the socket (shutdown read/write)
        # and that's also a cause when we receive an empty message
        print("we came into this except bit")
        return False

while True:

    # Calls Unix select() system call or Windows select() WinSock call with three parameters:
    #   - rlist - sockets to be monitored for incoming data
    #   - wlist - sockets for data to be send to (checks if for example buffers are not full and socket is ready to send some data)
    #   - xlist - sockets to be monitored for exceptions (we want to monitor all sockets for errors, so we can use rlist)
    # Returns lists:
    #   - reading - sockets we received some data on (that way we don't have to check sockets manually)
    #   - writing - sockets ready for data to be send thru them
    #   - errors  - sockets with some exceptions
    # This is a blocking call, code execution will "wait" here and "get" notified in case any action should be taken
    read_sockets, _, exception_sockets = select.select(sockets_list, [], sockets_list)

    # Iterate over notified sockets
    for notified_socket in read_sockets:
        print("\n socketssss: ", notified_socket, "-----------\n")
        # If notified socket is a server socket - new connection, accept it
        if notified_socket == server_socket:

            # Accept new connection
            # That gives us new socket - client socket, connected to this given client only, it's unique for that client

            # The other returned object is ip/port set

            mycolUser = mydb1["UserAccounts1"]
            
            client_socket, client_address = server_socket.accept()
            print("\n ---------------client socket: ", client_socket, "----------------\n")

            typeRecv = ""
            string1Recv = ""
            string2Recv = ""

            while True:
                typeRecv = client_socket.recv(1024).decode()
                
                string1Recv = client_socket.recv(1024).decode()

                string2Recv = client_socket.recv(1024).decode()
                
                print("typeRecv:", typeRecv)
                print("string1:", string1Recv)
                print("string2:", string2Recv)

                if typeRecv == "register":
                    username = string1Recv
                    password = string2Recv
                    userStatus = 1
                    userExistCheck = False
                    usernameQuery = {"username": username}
                    mydoc = mycolUser.find(usernameQuery)
                    for x in mydoc:
                        print("User exists")
                        userExistCheck = True
                    if userExistCheck == False:
                        addUser = { "username": username, "password": password, "Status": userStatus}
                        x = mycolUser.insert_one(addUser)
                       
                        print("user added")
                        snd = "registerpass"
                        client_socket.send(snd.encode())
                        checkUser = True
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(blank.encode())
                        client_socket.recv(1024)
                        client_socket.send(blank.encode())
                    else:
                        snd = "registerfail"
                        client_socket.send(snd.encode())
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(blank.encode())
                        client_socket.recv(1024)
                        client_socket.send(blank.encode())


                elif typeRecv == "login":
                    username = string1Recv
                    password = string2Recv
                    checkUser = False
                    usernameQuery = {"username": username, "password": password}
                    mydoc = mycolUser.find(usernameQuery)
                    for x in mydoc:
                        checkUser = True
                        print("user found")
                    if checkUser == False:
                        print("user not found")
                        snd = "loginfail"
                        client_socket.send(snd.encode())
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(blank.encode())
                        client_socket.recv(1024)
                        client_socket.send(blank.encode()) 
                        
                    elif checkUser == True:
                        print("user is found")
                        snd = "loginpass"
                        client_socket.send(snd.encode())
                        
                        client_socket.recv(1024)

                        sendUser = username
                        
                        client_socket.send(sendUser.encode())
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(blank.encode())

                elif typeRecv == "friendsearch":
                    frienduserName = string1Recv
                    checkUser = False
                    usernameQuery = {"username": frienduserName}
                    mydoc = mycolUser.find(usernameQuery)
                    for x in mydoc:
                        checkUser = True
                        print("user found")
                    if checkUser == False:
                        print("user not found")
                        snd = "friendfail"
                        client_socket.send(snd.encode())
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(blank.encode())
                        client_socket.recv(1024)
                        client_socket.send(blank.encode())

                    elif checkUser == True:
                        print("user is found")
                        snd = "friendpass"
                        client_socket.send(snd.encode())
                        
                        client_socket.recv(1024)

                        sendUser = frienduserName
                        
                        client_socket.send(sendUser.encode())
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(blank.encode())

                elif typeRecv == "addfriend":
                    userfriendsCol = mydb1["friends"]
                    username = string1Recv
                    frienduserName = string2Recv

                    friendexistCheck = False
                    friendQuery = {"username": username, "friendName": frienduserName}
                    mydoc = userfriendsCol.find(friendQuery)
                    for x in mydoc:
                        print("Friend exists")
                        friendexistCheck = True
                    if friendexistCheck == False:
                        addUser = { "username": username, "friendName": frienduserName}
                        x = userfriendsCol.insert_one(addUser)
                       
                        print("friend added")
                        snd = "friendaddpass"
                        client_socket.send(snd.encode())
                        checkUser = True
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(frienduserName.encode())
                        client_socket.recv(1024)
                        client_socket.send(username.encode())

                    else:
                        snd = "friendaddfail"
                        client_socket.send(snd.encode())
                        client_socket.recv(1024)
                        blank = "e"
                        client_socket.send(frienduserName.encode())
                        client_socket.recv(1024)
                        client_socket.send(username.encode())

                elif typeRecv == "friendshow":
                    userfriendsCol = mydb1["friends"]
                    username = string1Recv
                    queryF = {"username": username}
                    totalDoc = userfriendsCol.find(queryF).count()
                    print("number of docs: ", totalDoc)

                    snd = "friendCount"
                    client_socket.send(snd.encode())
                    client_socket.recv(1024)
                    totalDocStr = str(totalDoc)
                    client_socket.send(totalDocStr.encode())
                    client_socket.recv(1024)
                    blank = "e"
                    client_socket.send(blank.encode())

                elif typeRecv == "friendgetName":
                    userfriendsCol = mydb1["friends"]
                    username = string1Recv
                    totalcount = int(string2Recv)
                    friendNameArr = []

                    friendQuery = {"username": username}
                    mydoc = userfriendsCol.find(friendQuery)
                    i = 0
                    for x in mydoc:
                        friendQuery[i] = str(x["friendName"])
                        i += 1

                    snd = "friendNameRecv"
                    client_socket.send(snd.encode())
                    client_socket.recv(1024)
                    currentNum = str(totalcount)
                    client_socket.send(currentNum.encode())
                    if i == 0:
                        client_socket.send("You have no friends".encode())
                    else:

                        friendName = friendQuery[totalcount]
                        print("the name is: ", friendName)
                        client_socket.send(friendName.encode())

                elif typeRecv == "startChat":
                    
                    msgTo = string1Recv
                    message = string2Recv
                    
                    msgFrom = client_socket.recv(1024).decode()
                    print("message from:", msgFrom)
                    
                    mycolUsersChats = mydb1["UserChatMsg"]
                    
                    getChatsQuery = {"username": msgFrom, "friendName": msgTo}
                    getChats = mycolUsersChats.find(getChatsQuery)
                    
                    totalMsg = mycolUsersChats.find(getChatsQuery).count()
                    
                    client_socket.send(str(totalMsg).encode())
                    

                    newType = client_socket.recv(1024).decode()
                    print("a", newType)
                    msgToAns = client_socket.recv(1024).decode()
                    print("b", msgToAns)
                    client_socket.send("blank".encode())
                    messageAns = client_socket.recv(1024).decode()
                    print("c", messageAns)
                    client_socket.send("blank".encode())
                    msgFromAns = client_socket.recv(1024).decode()
                    print("d", msgFromAns)

                    if newType == "showMsgHist":

                        counter = int(client_socket.recv(1024).decode())
                        print("current count:", counter)

    

                



                        
                break
    
            
            

    # It's not really necessary to have this, but will handle some socket exceptions just in case
    for notified_socket in exception_sockets:

        # Remove from list for socket.socket()
        sockets_list.remove(notified_socket)

        # Remove from our list of users
        del clients[notified_socket]


